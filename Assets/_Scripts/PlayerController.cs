﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Photon.PunBehaviour
{



	// Use this for initialization
	void Start () {

        if (this.photonView.isMine)
        {
            this.GetComponentInChildren<Camera>().enabled = true;
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
