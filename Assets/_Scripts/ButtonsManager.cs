﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public class ButtonsManager : Photon.PunBehaviour
{

    public int myNumber = 0;

    PhotonView pv;
    private MyGameManager myGameManager;

    private GameObject button;

    // Use this for initialization
    void Start()
    {
        if (photonView.isMine)
        {

            myNumber = photonView.ownerId;
            ButtonManager();
        }

    }

    private void OnEnable()
    {
        MyGameManager.OnAgentArrived += MovePlayerToDestination_OnAgentArrived;
        MyGameManager.OnDiceRolled += RollDice_OnDiceRolled;
        MyGameManager.OnPlayerJoined += MyGameManager_OnPlayerJoined;
    }

    private void MyGameManager_OnPlayerJoined()
    {
        if (photonView.isMine)
        {
            myNumber = photonView.ownerId;
            ButtonManager();
        }
    }

    private void OnDisable()
    {
        MyGameManager.OnAgentArrived -= MovePlayerToDestination_OnAgentArrived;
        MyGameManager.OnDiceRolled -= RollDice_OnDiceRolled;
        MyGameManager.OnPlayerJoined -= MyGameManager_OnPlayerJoined;

    }

    private void RollDice_OnDiceRolled(int _diceValue)
    {
        if (photonView.isMine)
        {
            button.GetComponent<Button>().interactable = false;
        }
    }

    private void MovePlayerToDestination_OnAgentArrived(int _currentPosition)
    {
        if (photonView.isMine)
        {
            ButtonManager();
        }
    }

    private void ButtonManager()
    {
        myGameManager = GameObject.Find("GameManager").GetComponent<MyGameManager>();
        button = GameObject.Find("RollDiceButton");


        if (myGameManager.CurrentTurn == photonView.ownerId)
            button.GetComponent<Button>().interactable = true;
        else
            button.GetComponent<Button>().interactable = false;
    }
}
