﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : Photon.PunBehaviour
{


    public static List<KeyValuePair<int, List<CellType>>> PlayersProperties = new List<KeyValuePair<int, List<CellType>>>();
    private List<CellType> playerOneProperties = new List<CellType>();
    private List<CellType> playerTwoProperties = new List<CellType>();

    public static int Player1Wallet = 10;
    public static int Player2Wallet = 10;


    [PunRPC]
    void AssignPropertyToPlayer(CellType _cell, int _playerNumber, int _costProperty)
    {
        List<CellType> list = new List<CellType>();

        if (_playerNumber == 1)
        {
            playerOneProperties.Add(_cell);
            list = playerOneProperties;
            Player1Wallet -= _costProperty;
        }
        else
        {
            playerTwoProperties.Add(_cell);
            list = playerTwoProperties;
            Player2Wallet -= _costProperty;
        }

        PlayersProperties.Add(new KeyValuePair<int, List<CellType>>(_playerNumber, list));
    }

    [PunRPC]
    void PayRent(int _payingPlayer, int _receivingPlayer, int _rent)
    {
        if (_payingPlayer == 1)
        {
            Payment(Player1Wallet, Player2Wallet, _rent);
        }
        else
        {
            Payment(Player2Wallet, Player1Wallet, _rent);
        }

    }

    private void Payment(int _payingPlayer, int _receivingPlayer, int _rent)
    {
        if (_payingPlayer == 0)
            return;
        else
        {
            if (_payingPlayer < _rent)
            {
                _receivingPlayer += _payingPlayer;
                _payingPlayer = 0;
            }
            else
            {
                _receivingPlayer += _rent;
                _payingPlayer -= _rent;
            }
        }
    }

}
