﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCurrentMoney : Photon.PunBehaviour
{

    Text text;

    private void Start()
    {
        text = this.GetComponent<Text>();
        if (!PhotonNetwork.isMasterClient)
            text.text = PlayerInfo.Player1Wallet.ToString();
        else
            text.text = PlayerInfo.Player2Wallet.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        if (!PhotonNetwork.isMasterClient)
            text.text = PlayerInfo.Player1Wallet.ToString();
        else
            text.text = PlayerInfo.Player2Wallet.ToString();
    }
}
