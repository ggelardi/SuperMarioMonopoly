﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyGameManager : Photon.PunBehaviour
{

    public GameObject[] Spawns;
    public int CurrentTurn;
    public int MyPlayerID;
    public delegate void GameEventsHandler();
    public static event GameEventsHandler OnPlayerJoined;
    public delegate void DiceEventHandler(int _diceValue);
    public static event DiceEventHandler OnDiceRolled;
    public delegate void AgentEventHandler(int _currentPos);
    public static event AgentEventHandler OnAgentArrived;

    public GameObject[] Cells = new GameObject[32];
    public static List<KeyValuePair<CellType, bool>> CellState = new List<KeyValuePair<CellType, bool>>();

    public static MyGameManager Instance;

    PhotonView pv;

    private void Start()
    {
        Instance = this;
        InitialCellSetup();
        PhotonNetwork.ConnectUsingSettings("v1.0");
        pv = this.GetComponent<PhotonView>();

        if (PhotonNetwork.isMasterClient)
            MyPlayerID = 1;
        else
            MyPlayerID = 2;
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnRandomJoinFailed");
        RoomOptions roomOptions = new RoomOptions()
        {
            IsVisible = true,
            MaxPlayers = 6
        };

        PhotonNetwork.CreateRoom("MyRoom", roomOptions, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("JoinedRoom: " + PhotonNetwork.room.Name);

        if (PhotonNetwork.isMasterClient)
        {
            SpawnPlayer("Mario", Spawns[0]);
        }
        else
        {
            SpawnPlayer("Peach", Spawns[1]);
        }

        pv.RPC("PlayerJoined", PhotonTargets.All);
    }

    private void SpawnPlayer(string _playerName, GameObject _gameObject)
    {
        GameObject myPlayer = PhotonNetwork.Instantiate(_playerName, _gameObject.transform.position, _gameObject.transform.rotation, 0);
    }


    private void InitialCellSetup()
    {
        int count = Cells.Length;
        for (int i = 0; i < count; i++)
        {
            CellState.Add(new KeyValuePair<CellType, bool>(Cells[i].gameObject.GetComponent<EventManager>().cellType, false));
        }
    }


    [PunRPC]
    void DiceThrown(int _diceValue)
    {
        if (OnDiceRolled != null)
            OnDiceRolled(_diceValue);
    }

    [PunRPC]
    void PlayerJoined()
    {
        if (OnPlayerJoined != null)
            OnPlayerJoined();

        Debug.Log(PhotonNetwork.countOfPlayers + " " + PhotonNetwork.countOfPlayersInRooms);
    }

    [PunRPC]
    void AgentArrivedAtDestination(int _currentPosition)
    {
        if (OnAgentArrived != null)
            OnAgentArrived(_currentPosition);
    }

    [PunRPC]
    void Turn(int _currentTurn)
    {
        CurrentTurn = _currentTurn;
        //Debug.Log(CurrentTurn);
    }

    [PunRPC]
    void ChangeCellState(int _cellNumber, bool _wasSold)
    {
        CellType oldCellType = CellState[_cellNumber].Key;
        CellState[_cellNumber] = new KeyValuePair<CellType, bool>(oldCellType, false);
    }
}