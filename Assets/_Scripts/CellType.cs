﻿public enum CellType {

    START,
    BROWN_1,
    PIPE_1,
    BRWON_2,
    BOX_1,
    LIGHT_BLUE_1,
    STAR_1,
    LIGHT_BLUE_2,
    PRISON,
    PINK_1,
    PIPE_2,
    PINK_2,
    STUMP_1,
    ORANGE_1,
    STAR_2,
    ORANGE_2,
    PARKING,
    RED_1,
    PIPE_3,
    RED_2,
    BOX_2,
    YELLOW_1,
    STAR_3,
    YELLOW_2,
    POLICE,
    GREEN_1,
    PIPE_4,
    GREEN_2,
    STUMP_2,
    BLUE_1,
    STAR_4,
    BLUE_2
}
