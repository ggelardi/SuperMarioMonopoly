﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RollDice : Photon.PunBehaviour
{
    private int diceValue = 0;

    public float speed;
    private bool wasThrown = false;
    private bool rotate = false;
    public PhotonView pv;
    private MyGameManager myGameManager;
    private GameObject button;


    private void Start()
    {
        myGameManager = GameObject.Find("GameManager").GetComponent<MyGameManager>();
    }
    // Update is called once per frame
    void Update()
    {
        if (rotate)
            RotateDice();
    }

    private void OnEnable()
    {
        MyGameManager.OnPlayerJoined += MyGameManager_OnPlayerJoined;
    }

    private void OnDisable()
    {
        MyGameManager.OnPlayerJoined -= MyGameManager_OnPlayerJoined;
    }

    private void MyGameManager_OnPlayerJoined()
    {
        //if (PhotonNetwork.countOfPlayers < 2)
        //{
        //    button = GameObject.Find("RollDiceButton");
        //    button.GetComponent<Button>().interactable = false;
        //}
    }

    private void RotateDice()
    {
        if (!wasThrown)
            transform.Rotate(Vector3.one * speed * Time.deltaTime);
    }

    private void SetRotation(int _value)
    {
        switch (_value)
        {
            case 1:
                this.transform.localEulerAngles = new Vector3(180, 0, 0);
                break;
            case 2:
                this.transform.localEulerAngles = new Vector3(0, 270, 0);
                break;
            case 3:
                this.transform.localEulerAngles = new Vector3(90, 0, 0);
                break;
            case 4:
                this.transform.localEulerAngles = new Vector3(270, 0, 0);
                break;
            case 5:
                this.transform.localEulerAngles = new Vector3(0, 90, 0);
                break;
            case 6:
                this.transform.localEulerAngles = new Vector3(0, 0, 0);
                break;

            default:
                break;
        }
    }

    private void FireOnDiceRolledEvent()
    {
        this.transform.parent.GetComponent<Camera>().enabled = false;


        wasThrown = false;
        rotate = false;



        pv.RPC("DiceThrown", PhotonTargets.All, diceValue);

    }

    public void OnClickDown()
    {
        this.transform.parent.GetComponent<Camera>().enabled = true;
        rotate = true;

        button = GameObject.Find("RollDiceButton");
        button.GetComponent<Button>().interactable = false;

        Invoke("StopRotation", 2f);

    }

    private void StopRotation()
    {
        rotate = false;
        wasThrown = true;
        diceValue = Random.Range(1, 6);
        SetRotation(diceValue);
        Invoke("FireOnDiceRolledEvent", 1f);
    }
}
