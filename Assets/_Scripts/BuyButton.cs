﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BuyButton : MonoBehaviour {

    public GameObject BuyBtn;
    public GameObject DontBuyBtn;
    private PhotonView pv;
    private MyGameManager myGameManager;

    private int playerNumber;
    private int cellCost;
    private CellType cell;

	// Use this for initialization
	void Start () {
        myGameManager = GameObject.Find("GameManager").GetComponent<MyGameManager>();
        pv = GameObject.Find("GameManager").GetComponent<PhotonView>();
        SetBtnState(false);

	}

    private void OnEnable()
    {
        EventManager.OnCellCanBeBought += EventManager_OnCellCanBeBought;
    }
    private void OnDisable()
    {
        EventManager.OnCellCanBeBought -= EventManager_OnCellCanBeBought;
    }

    private void EventManager_OnCellCanBeBought(int _cellNumber, int _myPlayerID)
    {
        if(myGameManager.CurrentTurn == _myPlayerID)
        {
            cellCost = EventManager.PropertyCost((CellType)_cellNumber);
            playerNumber = myGameManager.CurrentTurn;
            cell = (CellType)_cellNumber;
            SetBtnState(true);
        }
    }



    void SetBtnState(bool _state)
    {

        BuyBtn.GetComponent<Image>().enabled = _state;
        DontBuyBtn.GetComponent<Image>().enabled = _state;

        BuyBtn.GetComponentInChildren<Text>().enabled = _state;
        DontBuyBtn.GetComponentInChildren<Text>().enabled = _state;
    }

    public void OnPropertyBuy()
    {
        pv.RPC("AssignPropertyToPlayer", PhotonTargets.All, cell,playerNumber, cellCost);
        SetBtnState(false);

    }

    public void OnPropertyDontBuy()
    {

        SetBtnState(false);
    }

}
