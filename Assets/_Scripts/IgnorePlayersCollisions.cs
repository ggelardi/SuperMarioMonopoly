﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnorePlayersCollisions : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Physics.IgnoreCollision(collision.collider, this.GetComponent<Collider>());
        }
    }
}
