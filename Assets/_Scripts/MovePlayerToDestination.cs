﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using DG.Tweening;

public class MovePlayerToDestination : Photon.PunBehaviour
{

    private NavMeshAgent agent;
    public GameObject Positions;
    PhotonView pv;
    public List<Transform> destinations = new List<Transform>();
    private MyGameManager myGameManager;
    private bool startCheckingDistance = false;
    private int myNumber = 0;

    private int currentPos = 0;
    
    private void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        pv = GameObject.Find("GameManager").GetComponent<PhotonView>();
        myGameManager = GameObject.Find("GameManager").GetComponent<MyGameManager>();
        Positions = GameObject.Find("Positions");

        if (photonView.isMine)
        {
            myNumber = photonView.ownerId;
            for (int i = 0; i < Positions.transform.childCount; i++)
            {
                destinations.Add(Positions.transform.GetChild(i).transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.isMine)
        {
            if (startCheckingDistance)
            {
                float distance = Vector3.Distance(agent.transform.position, destinations[currentPos].transform.position);


                if (distance <= 0.3f)
                {
                    print("Arrived at destination");

                    pv.RPC("AgentArrivedAtDestination", PhotonTargets.All, currentPos);

                    startCheckingDistance = false;
                }

            }
        }
    }

    private void OnEnable()
    {
        MyGameManager.OnDiceRolled += RollDice_OnDiceRolled;
    }

    private void OnDisable()
    {
        MyGameManager.OnDiceRolled -= RollDice_OnDiceRolled;
    }


    private void RollDice_OnDiceRolled(int _diceValue)
    {

        if (photonView.isMine)
        {
            //print(myGameManager.CurrentTurn + " " + photonView.ownerId);
            if (myGameManager.CurrentTurn == photonView.ownerId)
            {

                print(currentPos + _diceValue + " DestinationCount" + destinations.Count);
                if (currentPos + _diceValue > destinations.Count -1)
                {
                    int val = (currentPos + _diceValue) - destinations.Count;
                    agent.SetDestination(destinations[val].position);
                    currentPos = val;
                }
                else
                {
                    agent.SetDestination(destinations[currentPos + _diceValue].position);
                    currentPos += _diceValue;
                }


                startCheckingDistance = true;
            }
        }
    }

}
