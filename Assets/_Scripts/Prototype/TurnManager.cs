﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : Photon.PunBehaviour
{

    private int currentTurn = 0;

    private PhotonView pv;
    private PhotonView pv_1;
    
    // Use this for initialization
    void Start () {
        pv = GameObject.Find("GameManager").GetComponent<PhotonView>();
    }
    
    private void OnEnable()
    {
        MyGameManager.OnDiceRolled += MyGameManager_OnDiceRolled;
        MyGameManager.OnPlayerJoined += MyGameManager_OnPlayerJoined;
        MyGameManager.OnAgentArrived += MyGameManager_OnAgentArrived;
    }

    private void OnDisable()
    {
        MyGameManager.OnDiceRolled -= MyGameManager_OnDiceRolled;
        MyGameManager.OnPlayerJoined -= MyGameManager_OnPlayerJoined;
        MyGameManager.OnAgentArrived -= MyGameManager_OnAgentArrived;
    }

    private void MyGameManager_OnPlayerJoined()
    {
        currentTurn = 1;


        pv.RPC("Turn", PhotonTargets.All, currentTurn);

    }
    private void MyGameManager_OnAgentArrived(int _currentPosition)
    {

        currentTurn += 1;

        if (currentTurn == 3)//Set to number of players + 1 (if num of expected players == 2 use 3)
            currentTurn = 1;

        pv.RPC("Turn", PhotonTargets.All, currentTurn);

    }

    private void MyGameManager_OnDiceRolled(int _diceValue)
    {

    }
}
