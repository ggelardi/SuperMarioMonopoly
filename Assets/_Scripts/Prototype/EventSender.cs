﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSender : Photon.PunBehaviour
{

    PhotonView pv;

    private void Start()
    {
        pv = this.GetComponent<PhotonView>();
        string name = "Cube" + photonView.ownerId.ToString();
        pv.RPC("Receive", PhotonTargets.All, name);

    }




    [PunRPC]
    void Receive(string _name)
    {
        Debug.Log(_name);
    }

}
