﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class RotatePlayer : MonoBehaviour {

    public Vector3 Rotation;
    public float Speed = 1;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.transform.DOLocalRotate(Rotation, 1);
        }
    }
}
