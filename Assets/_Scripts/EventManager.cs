﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : Photon.PunBehaviour
{


    public CellType cellType;
    private PhotonView pv;
    private MyGameManager myGameManager;

    public delegate void CellEventHandler(int _cellNumber, int _currentPlayer);
    public static event CellEventHandler OnCellCanBeBought;
    private int[] interactableCells = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31 };

    private void Start()
    {
        pv = GameObject.Find("GameManager").GetComponent<PhotonView>();
        myGameManager = GameObject.Find("GameManager").GetComponent<MyGameManager>();

    }

    private void OnEnable()
    {
        MyGameManager.OnAgentArrived += MyGameManager_OnAgentArrived;
    }
    private void OnDisable()
    {
        MyGameManager.OnAgentArrived -= MyGameManager_OnAgentArrived;
    }
    private void MyGameManager_OnAgentArrived(int _currentPosition)
    {
        if (_currentPosition == (int)cellType)
        {
            CheckCellCurrentState(_currentPosition);
        }
    }


    private void CheckCellCurrentState(int _cellNumber)
    {
        //Was the cell sold already?
        print("Cell sold: " + MyGameManager.CellState[_cellNumber].Value);
        if (MyGameManager.CellState[_cellNumber].Value)
        {
            CellType selectedCell = (CellType)_cellNumber;
            // implement function to pay rent to other player

            if (!PlayerInfo.PlayersProperties[myGameManager.CurrentTurn - 1].Value.Contains(selectedCell)) //this player doesn't own this property
            {
                if (myGameManager.CurrentTurn == 1)
                    pv.RPC("PayRent", PhotonTargets.All, 1, 2, PropertyCost((CellType)_cellNumber));
                else
                    pv.RPC("PayRent", PhotonTargets.All, 2, 1, PropertyCost((CellType)_cellNumber));
            }

        }
        else
        {
            // Implement: ask if player wants to buy property
            print("Is Interactable: " + interactableCells.Contains(_cellNumber));

            if (interactableCells.Contains(_cellNumber + 1))
            {
                print("Return " + _cellNumber);
                return;
            }

            if (PlayerCanBuy(_cellNumber))
            {
                print("Fire: " + (_cellNumber));

                if (OnCellCanBeBought != null)
                    OnCellCanBeBought(_cellNumber, myGameManager.MyPlayerID);
            }

        }
    }

    bool PlayerCanBuy(int _cellNumber)
    {
        if (myGameManager.CurrentTurn == 1)
        {
            if (PlayerInfo.Player1Wallet >= PropertyCost((CellType)_cellNumber))
                return true;
            else
                return false;
        }
        else
        {
            if (PlayerInfo.Player2Wallet >= PropertyCost((CellType)_cellNumber))
                return true;
            else
                return false;
        }

    }


    public static int PropertyCost(CellType _cellType)
    {
        int rent = 0;

        switch (_cellType)
        {

            case CellType.BROWN_1:
                rent = 1;
                break;

            case CellType.BRWON_2:
                rent = 1;
                break;

            case CellType.LIGHT_BLUE_1:
                rent = 1;
                break;

            case CellType.LIGHT_BLUE_2:
                rent = 1;
                break;
            case CellType.PRISON:

            case CellType.PINK_1:
                rent = 2;
                break;

            case CellType.PINK_2:
                rent = 2;
                break;

            case CellType.ORANGE_1:
                rent = 2;
                break;

            case CellType.ORANGE_2:
                rent = 2;
                break;
            case CellType.RED_1:
                rent = 3;
                break;

            case CellType.RED_2:
                rent = 3;
                break;

            case CellType.YELLOW_1:
                rent = 3;
                break;

            case CellType.YELLOW_2:
                rent = 3;
                break;

            case CellType.GREEN_1:
                rent = 4;
                break;

            case CellType.GREEN_2:
                rent = 4;
                break;

            case CellType.BLUE_1:
                rent = 5;
                break;

            case CellType.BLUE_2:
                rent = 5;
                break;
            default:
                break;
        }

        return rent;
    }
}
